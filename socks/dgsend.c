#include "netinet/in.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "sys/socket.h"
#include "sys/types.h"
#include "unistd.h"

#define ooops(m) \
  {              \
    perror(m);   \
    exit(1);     \
  }

int make_dgram_client_socket();
int make_internet_address(char *host, int port, struct sockaddr_in *addrp);

int main(int ac, char *av[]) {
  int sock;
  if ((sock = make_dgram_client_socket()) == -1) {
    ooops("make_dgram_client_socket");
  }

  struct sockaddr_in saddr;
  if (make_internet_address(av[1], atoi(av[2]), &saddr) == -1) {
    ooops("make_internet_address");
  }

  char *msg = av[3];
  if (sendto(sock, msg, strlen(msg), 0, (struct sockaddr *)&saddr,
             sizeof(saddr)) == -1) {
    ooops("sendto");
  }

  char buf[BUFSIZ];
  if (recvfrom(sock, buf, BUFSIZ, 0, NULL, NULL) == -1) {
    ooops("recvfrom");
  }
  printf("dgrecv: got a message: %s\n", buf);
  return 0;
}