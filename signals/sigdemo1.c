#include "signal.h"
#include "stdio.h"
#include "unistd.h"

// Signal handler
// 当进程接收到这个信号，内核会调用函数f来处理这个信号
// 进程会跳转到那个函数，执行完毕后再返回到原来的地方
void signal_handler(int signo) { printf("Received signal %d\n", signo); }

int main() {
  signal(SIGINT, signal_handler);
  printf("Waiting for signal SIGINT\n");

  while (1) {
    sleep(1);
  }
  return 0;
}