#include "unistd.h"

// pipe(fd[2])
// write to fd[1]
// read from fd[0]

int main() {
  int fd[2];
  pipe(fd);

  if (fork() == 0) {
    // child
    // fd will be copied to child
    // which is different from parent
    close(fd[0]);
    write(fd[1], "hello", 5);
    close(fd[1]);
  } else {
    // parent
    close(fd[1]);
    char buf[5];
    read(fd[0], buf, 5);
    close(fd[0]);
    write(1, buf, 5);
  }
  return 0;
}