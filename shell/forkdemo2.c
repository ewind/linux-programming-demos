#include "stdio.h"
#include "unistd.h"

int main() {
  printf("before: my pid is %d\n", getpid());
  fork();
  fork();
  fork();
  printf("my pid is %d\n", getpid());
  return 0;
}

// Output:
// before: my pid is 42124
// my pid is 42124
// my pid is 42128
// my pid is 42125
// my pid is 42129
// my pid is 42127
// my pid is 42126
// my pid is 42130
// my pid is 42131