#include "stdio.h"
#include "unistd.h"

int main() {
  int ret_from_fork, mypid;
  mypid = getpid();

  printf("Before: my pid is %d\n", mypid);
  ret_from_fork = fork();
  sleep(1);
  printf("After: my pid is %d, fork() said %d\n", getpid(), ret_from_fork);
}

// Output:
// Before: my pid is 40421
// After: my pid is 40421, fork() said 40422
// After: my pid is 40422, fork() said 0