#include "stdio.h"
#include "unistd.h"

int main() {
  char *arglist[3];
  arglist[0] = "ls";  // first string is the command's name
  arglist[1] = "-l";
  arglist[2] = 0;  // last element must be 0

  printf("About to run ls -l\n");
  execvp("ls", arglist);
  // will not reach here if execvp is successful
  printf("ls is done\n");
}