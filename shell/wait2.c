#include "stdio.h"
#include "stdlib.h"
#include "sys/wait.h"
#include "unistd.h"

#define DELAY 5

void child_code(int delay) {
  printf("child %d here. will sleep for %d second\n", getpid(), delay);
  sleep(delay);
  printf("child done. about to exit\n");
  exit(17);
}

void parent_code(int childpid) {
  int wait_rv;
  int child_status;
  wait_rv = wait(&child_status);

  int high8 = child_status >> 8;
  int low7 = child_status & 0x7F;
  int bit7 = child_status & 0x80;
  printf("done waiting for %d. Wait returned: %d\n", childpid, wait_rv);
  printf("status: exit=%d, sig=%d, core=%d\n", high8, low7, bit7);
}

int main() {
  int newpid = 0;
  if ((newpid = fork()) == -1) {
    perror("fork");
  } else if (newpid == 0) {
    child_code(DELAY);
  } else {
    parent_code(newpid);
  }
}

// Output:
// child 48157 here. will sleep for 5 second
// child done. about to exit
// done waiting for 48157. Wait returned: 48157
// status: exit=17, sig=0, core=0