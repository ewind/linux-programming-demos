#include "sys/wait.h"

#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

void child_code() {
  printf("child %d here. will sleep for a second\n", getpid());
  sleep(1);
  printf("child done. about to exit\n");
  exit(17);
}

void parent_code(int childpid) {
  int wait_rv;
  wait_rv = wait(NULL);
  printf("done waiting for %d. Wait returned: %d\n", childpid, wait_rv);
}

int main() {
  int newpid = 0;
  if ((newpid = fork()) == -1) {
    perror("fork");
  } else if (newpid == 0) {
    child_code();
  } else {
    parent_code(newpid);
  }
}

// Output:
// child 45651 here. will sleep for a second
// child done. about to exit
// done waiting for 45651. Wait returned: 45651